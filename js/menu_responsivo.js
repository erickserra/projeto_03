$(function(){


	function openNav(){
		$('.sidenav').css('width','50%');
	}

	function closeNav(){
		$('.sidenav').css('width','0');
	}

	$('.sidenavBtn').click(function(){
		openNav();
	});

	$('.sidenav a').click(function(){
		closeNav();
	});

	$('.closeBtn').click(function(){
		closeNav();
	});


});